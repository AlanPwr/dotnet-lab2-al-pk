﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Grid
    {
        // ############## PRYWATNE PROPERTY ##############
        int[,] actuallyGeneration;  //przechowuje stan obecnych komorek, obecnej generacji
        int[,] previousGeneration;      //przechowuje stan poprzedniej generacji
        int counter;             //ilosc przeiterowanych generacji

        int h;  //as height
        int w;  //as width

        // ############## METODY ##############
        public Grid(int[,] newGrid)     //konstruktor
        {
            actuallyGeneration = (int[,])newGrid.Clone();   //tworzy generacje
            counter = 1;

            h = actuallyGeneration.GetLength(0);    // (y,x)
            w = actuallyGeneration.GetLength(1);
            previousGeneration = new int[h, w];     //pusta siatka do przechowywania poprzedniej generacji
        }

        public int Counter //getter, zwraca ilość przeiterowanych generacji
        {
            get
            {
                return counter;
            }
        }

        private int NeighboursAroundOneCell(int x, int y)   //liczy ile sasiadow ma jedna konkretna komorka
        {
            int count = 0;

            // Sprawdza dla x - 1, y - 1
            if (x > 0 && y > 0)
            {
                if (actuallyGeneration[y - 1, x - 1] == 1)
                    count++;
            }

            // Sprawdza dla x, y - 1
            if (y > 0)
            {
                if (actuallyGeneration[y - 1, x] == 1)
                    count++;
            }

            // Sprawdza dla x + 1, y - 1
            if (x < w - 1 && y > 0)
            {
                if (actuallyGeneration[y - 1, x + 1] == 1)
                    count++;
            }

            // Sprawdza dla x - 1, y
            if (x > 0)
            {
                if (actuallyGeneration[y, x - 1] == 1)
                    count++;
            }

            // Sprawdza dla x + 1, y
            if (x < w - 1)
            {
                if (actuallyGeneration[y, x + 1] == 1)
                    count++;
            }

            // Sprawdza dla x - 1, y + 1
            if (x > 0 && y < h - 1)
            {
                if (actuallyGeneration[y + 1, x - 1] == 1)
                    count++;
            }

            // Sprawdza dla x, y + 1
            if (y < h - 1)
            {
                if (actuallyGeneration[y + 1, x] == 1)
                    count++;
            }

            // Sprawdza dla x + 1, y + 1
            if (x < w - 1 && y < h - 1)
            {
                if (actuallyGeneration[y + 1, x + 1] == 1)
                    count++;
            }
            return count;
        }

        public void WriteNeighboursAroundOneCell()  //leci po kazdej komorce i wyswietla numer sasiadow wokol niej
        {
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    Console.Write("{0}", NeighboursAroundOneCell(x, y));
                }
                Console.WriteLine();
            }
        }

        public void MainGenerationLife()     //glowna logika zadania
        {
            int[,] newGeneration = new int[h, w];   //tworzymy zmienna zeby przetrzymala nastepny stan - nastepna generacje

            previousGeneration = (int[,])actuallyGeneration.Clone();    //poprzednia generacja jest klonem aktualnej generacji

            counter++;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    if (NeighboursAroundOneCell(x, y) < 2)  //osamotnienie - zywa komorka umiera
                        newGeneration[y, x] = 0;
                    else if (actuallyGeneration[y, x] == 0 && NeighboursAroundOneCell(x, y) == 3)   //reprodukcja
                        newGeneration[y, x] = 1;
                    else if (actuallyGeneration[y, x] == 1 &&
                            (NeighboursAroundOneCell(x, y) == 2 || NeighboursAroundOneCell(x, y) == 3)) //przezywanko
                        newGeneration[y, x] = 1;
                    else    //przeludnienie
                        newGeneration[y, x] = 0;
                }
            }
            actuallyGeneration = (int[,])newGeneration.Clone();
        }

        public void WriteGeneration()   //wyswietla cala siatke - generacje
        {
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    Console.Write("{0}", actuallyGeneration[y, x]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public int CountAliveCells()     //zlicza ilosc komorek które przezyly
        {
            int count = 0;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    if (actuallyGeneration[y, x] == 1)
                        count++;
                }
            }
            return count;
        }
    }
}
