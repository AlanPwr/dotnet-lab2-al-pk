﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main()
        {
            
            int[,] initialGrid = new int[30, 30]; // 
            Random rnd = new Random();
                                            // losowanie komorek na planszy
            for (int i = 0; i < initialGrid.GetLength(1); i++)
                for (int j = 0; j < initialGrid.GetLength(1); j++)
                    initialGrid[i, j] = rnd.Next(0, 2);  
                
            
            Grid myGrid = new Grid(initialGrid);
            Console.WriteLine("Generacja nr 0.");
            myGrid.WriteGeneration();
            Console.WriteLine();

            Console.WriteLine("Przycisnij Enter, zeby wczytac nastepna generacje.");
            Console.ReadLine();
            while (myGrid.CountAliveCells() > 0)
            {
                string ans;
                Console.Clear();
                Console.Beep();
                Console.WriteLine();
                Console.WriteLine("Generacja nr {0}.", myGrid.Counter);

                myGrid.MainGenerationLife();
                myGrid.WriteGeneration();

                Console.WriteLine();

                if (myGrid.CountAliveCells() == 0)
                {
                    Console.WriteLine("Every one died!");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Przycisnij Enter, zeby wczytac nastepna generacje.");
                    Console.WriteLine("Aby przerwac dzialanie programu wcisnij Q.");
                    ans = Console.ReadLine();

                    if (ans == "q" || ans == "Q")
                    {
                        Console.WriteLine("Koniec dzialania programu.");
                        Console.Beep();
                        break;
                    }
                }
            }
        }
    }
}
